from guardian import GuardState

from .. import const as top_const
from .. import decorators as dec
from . import util as ms_util
from ..damping import util as damp_util
from ..isolation import util as iso_util
from ..isolation import const as iso_const


class MASTERSWITCH_OFF(GuardState):
    request = False
    index = 25

    @dec.watchdog_is_not_tripped
    def main(self):
        #notify('Masterswitch is off.')
        iso_util.clear_iso_filters()
        if top_const.CHAMBER_TYPE != 'HPI':
            damp_util.clear_damp_filters()
        if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1')  & (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.switch_gs13_gain('LOW',iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
        if (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.turn_FF('OFF', iso_const.ISOLATION_CONSTANTS['FF_DOF'])
        
    @dec.watchdog_is_not_tripped
    def run(self):
        #notify('Masterswitch is off.')
        return ms_util.is_masterswitch_on()


class READY(GuardState):
    request = True
    index = 30

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_off
    @dec.isolation_loops_are_off
    def run(self):
        return True
